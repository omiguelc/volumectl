package main

import (
	"fmt"
	"os"
	"strconv"

	"github.com/urfave/cli"
	"github.com/omiguelc/volumectl/pulseaudio"
)

func main() {
	pa := pulseaudio.New()

	app := cli.NewApp()
	app.Name = "volumectl"
	app.Usage = "Control volume from the command line"
	app.Version = "0.1.0"
	app.Commands = []cli.Command{
		// SINK OPTIONS
		{
			Name:  "sink-up",
			Usage: "increase sink volume (with 2%)",
			Action: func(c *cli.Context) {
				pa.IncreaseSinkVolume()
				showVolumeNotification(pa.SinkVolume, pa.SinkMuted)
			},
		},
		{
			Name:  "sink-down",
			Usage: "decrease sink volume (with 2%)",
			Action: func(c *cli.Context) {
				pa.DecreaseSinkVolume()
				showVolumeNotification(pa.SinkVolume, pa.SinkMuted)
			},
		},
		{
			Name:  "sink-mute",
			Usage: "mute sink volume",
			Action: func(c *cli.Context) {
				pa.SetSinkMute(true)
				showVolumeNotification(pa.SinkVolume, pa.SinkMuted)
			},
		},
		{
			Name:  "sink-unmute",
			Usage: "unmute sink volume",
			Action: func(c *cli.Context) {
				pa.SetSinkMute(false)
				showVolumeNotification(pa.SinkVolume, pa.SinkMuted)
			},
		},
		{
			Name:  "sink-toggle",
			Usage: "toggle sink mute",
			Action: func(c *cli.Context) {
				pa.ToggleSinkMute()
				showVolumeNotification(pa.SinkVolume, pa.SinkMuted)
			},
		},
		{
			Name:  "sink-set",
			Usage: "set sink volume to a specific value",
			Action: func(c *cli.Context) {
				pa.SetSinkVolume(c.Args().First())
				showVolumeNotification(pa.SinkVolume, pa.SinkMuted)
			},
		},
		// SOURCE OPTIONS
		{
			Name:  "source-up",
			Usage: "increase source volume (with 2%)",
			Action: func(c *cli.Context) {
				pa.IncreaseSourceVolume()
				showVolumeNotification(pa.SourceVolume, pa.SourceMuted)
			},
		},
		{
			Name:  "source-down",
			Usage: "decrease source volume (with 2%)",
			Action: func(c *cli.Context) {
				pa.DecreaseSourceVolume()
				showVolumeNotification(pa.SourceVolume, pa.SourceMuted)
			},
		},
		{
			Name:  "source-mute",
			Usage: "mute source volume",
			Action: func(c *cli.Context) {
				pa.SetSourceMute(true)
				showVolumeNotification(pa.SourceVolume, pa.SourceMuted)
			},
		},
		{
			Name:  "source-unmute",
			Usage: "unmute source volume",
			Action: func(c *cli.Context) {
				pa.SetSourceMute(false)
				showVolumeNotification(pa.SourceVolume, pa.SourceMuted)
			},
		},
		{
			Name:  "source-toggle",
			Usage: "toggle source mute",
			Action: func(c *cli.Context) {
				pa.ToggleSourceMute()
				showVolumeNotification(pa.SourceVolume, pa.SourceMuted)
			},
		},
		{
			Name:  "source-set",
			Usage: "set source volume to a specific value",
			Action: func(c *cli.Context) {
				pa.SetSourceVolume(c.Args().First())
				showVolumeNotification(pa.SourceVolume, pa.SourceMuted)
			},
		},
	}
	app.Action = func(c *cli.Context) {
		mute := "[on]"
		if pa.SinkMuted {
			mute = "[off]"
		}
		fmt.Println("Volume:", strconv.Itoa(pa.SinkVolume)+"%", mute)
	}

	app.Run(os.Args)
}

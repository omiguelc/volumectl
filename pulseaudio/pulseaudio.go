package pulseaudio

import (
	"fmt"
	"log"
	"os/exec"
	"regexp"
	"strconv"
	"strings"
)

// PulseAudio client
type PulseAudio struct {
	defaultSink string
	defaultSource string

	SinkVolume int
	SinkMuted bool

	SourceVolume int
	SourceMuted bool
}

// New returns a new PulseAudio client
func New() *PulseAudio {
	defaultSink, defaultSource := detectDefaults()

	pa := &PulseAudio{defaultSink: defaultSink, defaultSource: defaultSource}
	pa.fetchSinkVolume()
	pa.fetchSourceVolume()

	return pa
}

// SetMute mutes or unmutes the default sink
func (pa *PulseAudio) SetSinkMute(muted bool) {
	mute := "0"

	if muted {
		mute = "1"
	}

	pa.SinkMuted = muted

	exec.Command("pactl", "set-sink-mute", pa.defaultSink, mute).Run()
}

func (pa *PulseAudio) SetSourceMute(muted bool) {
	mute := "0"

	if muted {
		mute = "1"
	}

	pa.SourceMuted = muted

	exec.Command("pactl", "set-source-mute", pa.defaultSource, mute).Run()
}

// Toggle*Mute toogles mute on the default sink
func (pa *PulseAudio) ToggleSinkMute() {
	pa.SinkMuted = !pa.SinkMuted
	exec.Command("pactl", "set-sink-mute", pa.defaultSink, "toggle").Run()
}

func (pa *PulseAudio) ToggleSourceMute() {
	pa.SourceMuted = !pa.SourceMuted
	exec.Command("pactl", "set-source-mute", pa.defaultSource, "toggle").Run()
}

// SetVolume sets the volume on the default sink
func (pa *PulseAudio) SetSinkVolume(volume string) {
	exec.Command("pactl", "set-sink-volume", pa.defaultSink, volume).Run()
	pa.fetchSinkVolume()
}

func (pa *PulseAudio) SetSourceVolume(volume string) {
	exec.Command("pactl", "set-source-volume", pa.defaultSource, volume).Run()
	pa.fetchSourceVolume()
}

// IncreaseVolume increases the volume on the default sink by 2%
func (pa *PulseAudio) IncreaseSinkVolume() {
	pa.SetSinkMute(false)
	volumeValue := "+2%"

	pa.SinkVolume += 2

	if pa.SinkVolume >= 98 {
		volumeValue = "100%"
		pa.SinkVolume = 100
	}

	exec.Command("pactl", "set-sink-volume", pa.defaultSink, volumeValue).Run()
}

func (pa *PulseAudio) IncreaseSourceVolume() {
	pa.SetSourceMute(false)
	volumeValue := "+2%"

	pa.SourceVolume += 2

	if pa.SourceVolume >= 98 {
		volumeValue = "100%"
		pa.SourceVolume = 100
	}

	exec.Command("pactl", "set-source-volume", pa.defaultSource, volumeValue).Run()
}

// DecreaseVolume decreases the volume on the default sink by 2%
func (pa *PulseAudio) DecreaseSinkVolume() {
	pa.SetSinkMute(false)
	volumeValue := "-2%"

	pa.SinkVolume -= 2

	if pa.SinkVolume < 0 {
		pa.SinkVolume = 0
	}

	exec.Command("pactl", "set-sink-volume", pa.defaultSink, volumeValue).Run()
}

func (pa *PulseAudio) DecreaseSourceVolume() {
	pa.SetSourceMute(false)
	volumeValue := "-2%"

	pa.SourceVolume -= 2

	if pa.SourceVolume < 0 {
		pa.SourceVolume = 0
	}

	exec.Command("pactl", "set-source-volume", pa.defaultSource, volumeValue).Run()
}

// INTERNAL
func detectDefaults() (string, string) {
	out, err := exec.Command("pactl", "info").Output()

	if err != nil {
		log.Fatalf("Unable to detect default sink: %s", err)
	}

	// DEFAULT SINK
	re := regexp.MustCompile(`Default Sink: (.*)`)
	res := re.FindSubmatch(out)

	if res == nil {
		log.Fatal("Unable to parse default sink name from pactl command output")
	}

	defaultSink := string(res[1][:])

	// DEFAULT SOURCE
	re = regexp.MustCompile(`Default Source: (.*)`)
	res = re.FindSubmatch(out)

	if res == nil {
		log.Fatal("Unable to parse default source name from pactl command output")
	}

	defaultSource := string(res[1][:])

	// RETURN
	return defaultSink, defaultSource
}

func (pa *PulseAudio) fetchSinkVolume() {
	// GET SINKS
	out, err := exec.Command("pactl", "list", "sinks").Output()

	if err != nil {
		log.Fatalf("pactl get volume command failed: %s", err)
	}

	// GET SINK
	currentSinkOutput, err := findSinkByName(out, pa.defaultSink)

	if err != nil {
		log.Fatal(err)
	}

	// FIND THE VOLUME BY IDENTIFYING A VOLUME WITH THE PATTERN BELOW
	// WE SHOULD BE CAREFUL ABOUT THIS, BECAUSE THERE ARE VARIOUS PERCENTAGES
	// IN THE OUTPUT, HOWEVER GVEN THAT IT'S THE FIRST PERCENTAGE IT'S
	// PROBABLY SAFE TO DO IT THIS WAY
	re := regexp.MustCompile(`(\d+)%`)
	res := re.FindSubmatch(currentSinkOutput)

	if res == nil {
		log.Fatal("Unable to parse volume from pactl command output")
	}

	// GET THE FIRST GROUP
	parsedVolume := string(res[1][:])
	pa.SinkVolume, err = strconv.Atoi(parsedVolume)

	if err != nil {
		log.Fatal("Unable to convert volume value")
	}

	pa.SinkMuted = regexp.MustCompile(`Mute: yes`).Match(currentSinkOutput)
}

func findSinkByName(output []byte, sinkName string) ([]byte, error) {
	sinks := strings.Split(string(output), "Sink #")

	for _, sinkOutput := range sinks {
		if strings.Contains(sinkOutput, fmt.Sprintf("Name: %s", sinkName)) {
			return []byte(sinkOutput), nil
		}
	}

	return nil, fmt.Errorf("Unable to find sink named %s in 'pactl list sinks` output", sinkName)
}

func (pa *PulseAudio) fetchSourceVolume() {
	// GET SINKS
	out, err := exec.Command("pactl", "list", "sources").Output()

	if err != nil {
		log.Fatalf("pactl get volume command failed: %s", err)
	}

	// GET SINK
	currentSourceOutput, err := findSourceByName(out, pa.defaultSource)

	if err != nil {
		log.Fatal(err)
	}

	// FIND THE VOLUME BY IDENTIFYING A VOLUME WITH THE PATTERN BELOW
	// WE SHOULD BE CAREFUL ABOUT THIS, BECAUSE THERE ARE VARIOUS PERCENTAGES
	// IN THE OUTPUT, HOWEVER GVEN THAT IT'S THE FIRST PERCENTAGE IT'S
	// PROBABLY SAFE TO DO IT THIS WAY
	re := regexp.MustCompile(`(\d+)%`)
	res := re.FindSubmatch(currentSourceOutput)

	if res == nil {
		log.Fatal("Unable to parse volume from pactl command output")
	}

	// GET THE FIRST GROUP
	parsedVolume := string(res[1][:])
	pa.SourceVolume, err = strconv.Atoi(parsedVolume)

	if err != nil {
		log.Fatal("Unable to convert volume value")
	}

	pa.SourceMuted = regexp.MustCompile(`Mute: yes`).Match(currentSourceOutput)
}

func findSourceByName(output []byte, sourceName string) ([]byte, error) {
	sources := strings.Split(string(output), "Source #")

	for _, sourceOutput := range sources {
		if strings.Contains(sourceOutput, fmt.Sprintf("Name: %s", sourceName)) {
			return []byte(sourceOutput), nil
		}
	}

	return nil, fmt.Errorf("Unable to find source named %s in 'pactl list sources` output", sourceName)
}
